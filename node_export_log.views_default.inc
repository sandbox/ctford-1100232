<?php

/**
 * Implementation of hook_views_data().
 */
function node_export_log_views_data() {
  
  $data = array();
  $data['node_export_log']['table']['group'] = t('Node Export');
  $data['node_export_log']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Node export log'),
    'help' => t("Records where nodes have been imported from."),
  );
  $data['node_export_log']['table']['join'] = array(
    'node' => array(
        'left_field' => 'nid',
        'field' => 'nid',
    ),
  );
  
  $data['node_export_log']['source_url'] = array(
    'title' => t('Source URL'),
    'help' => t('A URL pointing to the source location of the node.'),
    'field' => array(
      'handler' => 'views_handler_field_url',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['node_export_log']['timestamp'] = array(
    'title' => t('Timestamp'),
    'help' => t('The date/time that the node was imported.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );
  
  return $data;
}


function node_export_log_views_default_views() {

$view = new view;
$view->name = 'node_source';
$view->description = 'Displays where nodes have been imported from.';
$view->tag = '';
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = FALSE;
$view->api_version = '3.0-alpha1';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Defaults */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'source_url' => 'source_url',
  'timestamp' => 'timestamp',
);
$handler->display->display_options['style_options']['default'] = 'timestamp';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 0,
    'separator' => '',
  ),
  'source_url' => array(
    'sortable' => 0,
    'separator' => '',
  ),
  'timestamp' => array(
    'sortable' => 0,
    'separator' => '',
  ),
);
$handler->display->display_options['style_options']['override'] = 1;
$handler->display->display_options['style_options']['sticky'] = 0;
$handler->display->display_options['style_options']['order'] = 'desc';
/* Field: Node: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 1;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
/* Field: Node Export: Source URL */
$handler->display->display_options['fields']['source_url']['id'] = 'source_url';
$handler->display->display_options['fields']['source_url']['table'] = 'node_export_log';
$handler->display->display_options['fields']['source_url']['field'] = 'source_url';
$handler->display->display_options['fields']['source_url']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['source_url']['alter']['make_link'] = 1;
$handler->display->display_options['fields']['source_url']['alter']['path'] = '[source_url]';
$handler->display->display_options['fields']['source_url']['alter']['target'] = '_blank';
$handler->display->display_options['fields']['source_url']['alter']['trim'] = 0;
$handler->display->display_options['fields']['source_url']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['source_url']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['source_url']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['source_url']['alter']['html'] = 0;
$handler->display->display_options['fields']['source_url']['hide_empty'] = 0;
$handler->display->display_options['fields']['source_url']['empty_zero'] = 0;
$handler->display->display_options['fields']['source_url']['display_as_link'] = 0;
/* Field: Node Export: Timestamp */
$handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['table'] = 'node_export_log';
$handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
/* Sort criterion: Node Export: Timestamp */
$handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['sorts']['timestamp']['table'] = 'node_export_log';
$handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
$handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';
/* Filter: Node Export: Source URL */
$handler->display->display_options['filters']['source_url']['id'] = 'source_url';
$handler->display->display_options['filters']['source_url']['table'] = 'node_export_log';
$handler->display->display_options['filters']['source_url']['field'] = 'source_url';
$handler->display->display_options['filters']['source_url']['operator'] = '!=';
$handler->display->display_options['filters']['source_url']['expose']['operator'] = FALSE;
$handler->display->display_options['filters']['source_url']['case'] = 0;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page_1');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['path'] = 'node_export_log';

  return array($view->name => $view);
}
